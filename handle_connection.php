<?php

if ($_POST['username'] && $_POST['password']) {

    // Connexion à la base de données

    include('dbconnect.php');

    $sql = 'SELECT * FROM user WHERE username = ? AND password = ?';

    $user_sql = $database->prepare($sql);
    $user_sql->execute([
        $_POST['username'],
        sha1($_POST['password'])
    ]);
    $user = $user_sql->fetchAll();

    // Vérification des informations de connexion

    if ($user) {

        session_start();
        $_SESSION['username'] = $_POST['username'];
        $_SESSION['password'] = $_POST['password'];

        header ('location: read.php');

    } else {
        echo "Identifiants incorrects";
    }

} else {
    echo 'Veuillez entrer toutes les informations';
}

?>

