﻿<?php

$servername = 'localhost';
$username = 'root';
$password = 'MySQL_123';
$database = 'reunion_island';
$dsn = "mysql:dbname=$database;host=$servername";
$options = [
    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
];

try {
	$database = new PDO($dsn, $username, $password, $options);
} catch (PDOException $e) {
	echo "Échec de la connexion à la base de données : {$e->getMessage()}";
}

?>
