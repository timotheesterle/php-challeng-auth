<?php

session_start();

function check_login() {
    if (!$_SESSION) header('location: login.php');
}

check_login();

?>
